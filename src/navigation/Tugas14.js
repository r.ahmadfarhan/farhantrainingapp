import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createStackNavigator } from '@react-navigation/stack';
import SplashScreen from './../screens/SplashScreen';
import Login from './../screens/Login';
import Home from './../screens/Home';
import Maps from './../screens/Maps';
import Chat from './../screens/Chat';
import Profile from './../screens/Profile';
import Chart from './../screens/Chart';
import Icon from 'react-native-vector-icons/Ionicons';

const Tabs = createBottomTabNavigator();
const TabsScreen = () => (
    <Tabs.Navigator
        screenOptions={({ route }) => ({
            tabBarIcon: ({ focused, color, size }) => {
            let iconName;

            if (route.name === 'Home') {
                iconName = focused ? 'home' : 'home-outline';
            } else if (route.name === 'Maps') {
                iconName = focused ? 'location' : 'location-outline';
            } else if (route.name === 'Chat') {
                iconName = focused ? 'chatbubbles' : 'chatbubbles-outline';
            } else if (route.name === 'Profile') {
                iconName = focused ? 'person' : 'person-outline';
            }

            // You can return any component that you like here!
            return <Icon name={iconName} size={size} color={color} />;
            },
        })}
        tabBarOptions={{
            activeTintColor: '#088dc4',
            inactiveTintColor: '#000000',
        }}
    >
        <Tabs.Screen name="Home" component={Home} />
        <Tabs.Screen name="Maps" component={Maps} />
        <Tabs.Screen name="Chat" component={Chat} />
        <Tabs.Screen name="Profile" component={Profile} />
    </Tabs.Navigator>
)

const Stack = createStackNavigator();

const MainNavigation = () => (
    <Stack.Navigator>
        <Stack.Screen name="Login" component={Login} options={{ headerShown: false }} />
        <Stack.Screen name="Dashboard" component={TabsScreen} options={{ headerShown: false }} />
        <Stack.Screen name="Chart" component={Chart} />
    </Stack.Navigator>
)

function AppNavigation() {
    const [ isLoading, setIsLoading ] = React.useState(true)

    //mengatur durasi splashscreen saat aplikasi pertama kali dibuka
    React.useEffect(() => {
        setTimeout(() => {
            setIsLoading(!isLoading)
        }, 2000)
    }, [])

    if(isLoading) {
        return <SplashScreen />
    }

    return (
        <NavigationContainer>
            <MainNavigation />
        </NavigationContainer>
    )
}

export default AppNavigation;