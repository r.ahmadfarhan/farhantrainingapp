import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import SplashScreen from './../screens/SplashScreen';
import Home from './../screens/Home';
import Maps from './../screens/Maps';
import Profile from './../screens/Profile';
import Icon from 'react-native-vector-icons/Ionicons';

const Tabs = createBottomTabNavigator();

const MainNavigation = () => (
    <Tabs.Navigator
        screenOptions={({ route }) => ({
            tabBarIcon: ({ focused, color, size }) => {
            let iconName;

            if (route.name === 'Home') {
                iconName = focused ? 'home' : 'home-outline';
            } else if (route.name === 'Maps') {
                iconName = focused ? 'location' : 'location-outline';
            } else if (route.name === 'Profile') {
                iconName = focused ? 'person' : 'person-outline';
            }

            // You can return any component that you like here!
            return <Icon name={iconName} size={size} color={color} />;
            },
        })}
        tabBarOptions={{
            activeTintColor: '#088dc4',
            inactiveTintColor: '#000000',
        }}
    >
        <Tabs.Screen name="Home" component={Home} />
        <Tabs.Screen name="Maps" component={Maps} />
        <Tabs.Screen name="Profile" component={Profile} />
    </Tabs.Navigator>
)

function AppNavigation() {
    const [ isLoading, setIsLoading ] = React.useState(true)

    //mengatur durasi splashscreen saat aplikasi pertama kali dibuka
    React.useEffect(() => {
        setTimeout(() => {
            setIsLoading(!isLoading)
        }, 3000)
    }, [])

    if(isLoading) {
        return <SplashScreen />
    }

    return (
        <NavigationContainer>
            <MainNavigation />
        </NavigationContainer>
    )
}

export default AppNavigation;