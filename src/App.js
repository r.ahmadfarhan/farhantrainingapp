import React, { useEffect } from 'react';
import AppNavigation from './routes';
import firebase from '@react-native-firebase/app'; // import library firebase

//ganti isi dari firebaseConfig, sesuai dengan project yang anda buat di firebase
var firebaseConfig = {
    apiKey: "AIzaSyDUwuyzD0uGf9lCBlsJx2Nhm-843KS9jkg",
    authDomain: "sanbercode-baba2.firebaseapp.com",
    databaseURL: "https://sanbercode-baba2.firebaseio.com",
    projectId: "sanbercode-baba2",
    storageBucket: "sanbercode-baba2.appspot.com",
    messagingSenderId: "1070759017540",
    appId: "1:1070759017540:web:fae94e79a4381ae3555011",
    measurementId: "G-KM5T1XBLCF"
};
// Initialize Firebase
// firebase.initializeApp(firebaseConfig);
// firebase.analytics();

// Inisialisasi firebase
if(!firebase.apps.length) {
    firebase.initializeApp(firebaseConfig)
}

// const App: () => React$Node = () => {
const App = () => {

  return (
    <AppNavigation />
  );
};

export default App;