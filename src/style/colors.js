import { StyleSheet } from "react-native";

const colors = StyleSheet.create({
    blue: {
        color: '#088dc4',
    },
    darkblue: {
        color: '#3EC6FF',
    }
});

export default colors;
