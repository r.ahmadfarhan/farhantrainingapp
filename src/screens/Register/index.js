import React, { useEffect, useState } from 'react';
import {
    Text,
    TextInput,
    View,
    Modal,
    Image,
    StatusBar,
    TouchableOpacity
  } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import styles from './style';
import { RNCamera } from 'react-native-camera';
import storage from '@react-native-firebase/storage';
import Icon from 'react-native-vector-icons/Feather';
import MaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons'

function Register({ navigation }) {

    const [isVisible, setIsVisible] = useState(false)
    const [type, setType] = useState('back')
    const [photo, setphoto] = useState(null)

    const toggleCamera = () => {
        setType(type === 'back' ? 'front' : 'back')
    }

    const takePicture = async () => {
        const options = { quality: 0.5, base64: true }
        if (camera) {
            const data = await camera.takePictureAsync(options)
            console.log("takePicture -> data", data)
            setphoto(data)
            setIsVisible(false)
        }
    }

    const uploadImage = (uri) => {
        const sessionId = new Date().getTime()
        return storage()
        .ref(`images/${sessionId}`)
        .putFile(uri)
        .then((response) => {
            alert('Upload Success')
        })
        .catch((error) => {
            alert(error)
        })
    }

    const renderCamera = () => {
        return (
            <Modal visible={isVisible} onRequestClose={() => setIsVisible(false)}>
                <View style={{ flex: 1 }}>
                    <RNCamera
                        style={{flex:1}}
                        ref={ref => {
                            camera = ref;
                        }}
                        type={type}
                    >
                        <View style={styles.btnFlipContainer}>
                            <TouchableOpacity style={styles.btnFlip} onPress={() => toggleCamera()}>
                                <MaterialCommunity name="rotate-3d-variant" size={15} />
                            </TouchableOpacity>
                        </View>
                        <View style={styles.round} />
                        <View style={styles.rectangle} />
                        <View style={styles.btnTakeContainer}>
                            <TouchableOpacity style={styles.btnTake} onPress={() => takePicture()}>
                                <Icon name='camera' size={30} />
                            </TouchableOpacity>
                        </View>
                    </RNCamera>
                </View>
            </Modal>
        )
    }

    return (
        <View style={styles.container}>
            <StatusBar backgroundColor="#3EC6FF" barStyle="dark-content" />
            <ScrollView>
            <View style={styles.background}>
                <View style={styles.photo}>
                    <Image source={photo === null ? require('./../../assets/images/pp.jpg') : { uri: photo.uri }} style={{ width:100, height: 100, borderRadius: 50 }} />
                </View>
                <View style={styles.name}> 
                    <TouchableOpacity style={styles.changePicture} onPress={() => setIsVisible(true)}>
                        <Text style={styles.nameText}>Change picture</Text>
                    </TouchableOpacity>
                </View>
            </View>
            <View style={styles.box}>
                <View style={styles.formContainer}>
                    <Text>Nama</Text>
                    <TextInput
                        // value={name}
                        underlineColorAndroid= '#c6c6c6'
                        placeholder='Name'
                        // onChangeText={(email)=>setEmail(email)}
                    />
                    <Text>Email</Text>
                    <TextInput
                        // value={email}
                        underlineColorAndroid= '#c6c6c6'
                        placeholder='Email'
                        // onChangeText={(email)=>setEmail(email)}
                    />
                    <Text>Password</Text>
                    <TextInput
                        secureTextEntry
                        // value={password}
                        underlineColorAndroid= '#c6c6c6'
                        placeholder='Password'
                        // onChangeText={(password)=>setPassword(password)}
                    />
                </View>
                <View style={styles.btnContainer}>
                    <TouchableOpacity style={styles.register} onPress={() => uploadImage(photo.uri)}>
                        <Text style={{color: '#ffffff'}}>REGISTER</Text>
                    </TouchableOpacity>
                </View>
            </View>
            </ScrollView>
            {renderCamera()}
        </View>
    )
}
export default Register;
