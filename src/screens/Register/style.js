import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    background: {
        backgroundColor: '#3EC6FF',
        height: 250,
    },
    photo: {
        alignItems: 'center',
        marginTop: 50,
    },
    name: {
        alignItems: 'center',
    },
    nameText: {
        fontSize: 18,
        lineHeight: 21,
        fontWeight: 'bold',
        marginVertical: 10,
        color: '#FFFFFF'
    },
    box: {
        backgroundColor: '#FFFFFF',
        borderRadius: 10,
        marginTop: -40,
        marginHorizontal: 20,
        paddingVertical: 13,
        paddingHorizontal: 10,
        shadowColor: "#000",
        shadowOffset: {width: 1, height: 3,},
        shadowOpacity: 0.3,
        shadowRadius: 6,
        elevation: 10,
    },
    row: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginHorizontal: 20,
        marginVertical: 10
    },
    text: {
        fontSize: 14,
        color: '#000000'
    },
    formContainer: {
        marginVertical: 10,
        marginHorizontal: 20
    },
    btnContainer: {
        margin: 15,
        backgroundColor: '#3EC6FF',
        alignItems: 'center'
    },
    register: {
        fontSize: 14,
        marginVertical: 10
    }
});

export default styles;
