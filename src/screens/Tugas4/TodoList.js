
import React, {useContext} from 'react';
import { FlatList, StatusBar, StyleSheet, Text, TextInput, TouchableOpacity, View, Image, ScrollView } from 'react-native';
import { RootContext } from './index';

const TodoList = () => {
    const state = useContext(RootContext)
    
    console.log("todolist=>state", state)
    
    const renderItem =({item, index})=>{
        return (
            <View style={styles.containerItem}>
                <View style={styles.text}>
                    <Text>{item.date}</Text>
                    <Text>{item.text}</Text>
                </View>
                <TouchableOpacity onPress={() => state.removeTodo(index)} style={styles.delete}>
                    <Image source={require('./../../assets/icons//trash_red.png')} style={{ width:18, height: 22 }} />
                </TouchableOpacity>
            </View>
        )
    }
    return (
        <View style={styles.container}>
            <StatusBar backgroundColor='#FFFFFF' />
            <View style={styles.content}>
                <Text style={{marginBottom: 10}}>Masukan Todolist</Text>
                <TextInput
                     style={styles.inputText}
                     value={state.input}
                     placeholder="Input here"
                     onChangeText={(value) => state.handleChangeInput(value)}
                />
                <TouchableOpacity style={styles.add} onPress={() => state.addTodo()}>
                    <Text style={styles.addVector}>+</Text>
                </TouchableOpacity>
            </View>
            <ScrollView style={styles.scrollContainer}>
                <FlatList
                    data={state.todos}
                    renderItem={renderItem}
                />
            </ScrollView>
        </View>
    )
}
export default TodoList

const styles = StyleSheet.create({
    container:{
        flex: 1
    },
    content:{
        margin: 20,
    },
    inputText:{
        alignSelf: 'stretch',
        color: '#000',
        padding: 8,
        borderWidth: 1,
        marginRight: 55,
        borderWidth:1,
    }, 
    add:{
        position: 'absolute',
        backgroundColor: '#3EC6FF',
        width: 45,
        height: 45,
        justifyContent: 'center',
        alignItems: 'center',
        top: 30,
        bottom: 10,
        right: 0
    },
    addVector:{
        color: '#000',
        fontSize: 24,
    },
    scrollContainer: {
        flex: 1,
        marginBottom: 10,
    },
    containerItem: {
        position: 'relative',
        padding: 20,
        paddingRight: 100,
        borderBottomWidth: 2,
        borderBottomColor: '#ededed',
    },
    text: {
        paddingLeft: 20,
        borderLeftWidth: 10,
        borderLeftColor: '#3EC6FF',
    },
    delete: {
        position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10,
        top: 10,
        bottom: 10,
        right: 20
    },
})
