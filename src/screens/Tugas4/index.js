import React, { createContext, useState } from 'react';
import TodoList from './TodoList';

export const RootContext = createContext();

const Context = () => {
    const [input, setInput] = useState('')
    const [todos, setTodos] = useState([])

    handleChangeInput =(value)=>{
        setInput (value)
    }

    addTodo =()=>{
        const day = new Date().getDate()
        const month = new Date().getMonth() + 1
        const year = new Date ().getFullYear()
        const today = `${day}/${month}/${year}`

        if(input.length > 0){
            setTodos([...todos, {text: input, date: today}])
            setInput('')
        }
    }

    removeTodo = (id) => {
        setTodos(
            todos.filter((todo, index) => {
                console.log("removeTodo => index", index)
                console.log("removeTodo => todo", todo)
                if(index!==id){
                    return true
                }
            })
        )
    }

    return (
        <RootContext.Provider value={{
            input,
            todos,
            handleChangeInput,
            addTodo,
            removeTodo
        }}>
            <TodoList />
        </RootContext.Provider>
    )
}
export default Context;