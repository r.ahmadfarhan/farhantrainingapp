import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    slide: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    title: {
        fontWeight: 'bold',
        color: '#191970',
        fontSize: 22,
    },
    image: {
        width: 200,
        height: 200,
        marginVertical: 20
    },
    text: {
        color: '#a4a4a6',
        fontWeight: 'bold',
        fontSize: 16,
        textAlign: 'center',
        marginHorizontal: 20
    },
    buttonCircle: {
        borderRadius: 50,
        backgroundColor: '#191970',
        width: 50,
        height: 50,
        right:20,
        alignItems: 'center',
        justifyContent: 'center'
    },
})

export default styles;