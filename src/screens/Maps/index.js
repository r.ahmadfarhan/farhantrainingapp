import React from 'react';
import { View, Text, Image, StatusBar } from 'react-native';
// import styles from './style';
import MapboxGL from '@react-native-mapbox-gl/maps';
import { useEffect } from 'react/cjs/react.production.min';

MapboxGL.setAccessToken('pk.eyJ1Ijoici1haG1hZGZhcmhhbiIsImEiOiJja2c3dTR0azEwNzE3MndvM21lZ2NpMW40In0.ckkOAsZOk_D9Ctwp1kWNcg')

const Maps = () => {

    useEffect(() => {
        const getLocation = async() => {
            try {
                const permission = await MapboxGL.requestAndroidLocationPermissions()
            } catch(error) {
                console.log(error)
            }
        }
        getLocation()
    }, [])

    const coordinates = [
        [107.598827, -6.896191],
        [107.596198, -6.899688],
        [107.618767, -6.902226],
        [107.621095, -6.898690],
        [107.615698, -6.896741],
        [107.613544, -6.897713],
        [107.613697, -6.893795],
        [107.610714, -6.891356],
        [107.605468, -6.893124],
        [107.609180, -6.898013]
    ]

    return (
        <View style={styles.container}>
            <StatusBar backgroundColor="#FFFFFF" barStyle="dark-content" />
            <MapboxGL.MapView
                style={{ flex:1 }}
            >
                <MapboxGL.UserLocation 
                    visible={true}
                />
                <MapboxGL.Camera 
                    followUserLocation={true}
                />
                <MapboxGL.PointAnnotation 
                    id='pointAnnotation'
                    coordinate={coordinates}
                >
                    {/* <MapboxGL.Callout 
                        title='Ini Callout'
                    /> */}
                </MapboxGL.PointAnnotation>
            </MapboxGL.MapView>
            
        </View>
    );
}
export default Maps;
