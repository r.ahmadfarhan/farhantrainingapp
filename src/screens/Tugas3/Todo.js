import React from 'react';
import { View, Text, Image, TouchableOpacity, StyleSheet } from 'react-native';

const Todo = ({keyval, val, deleteMethod}) => {
    return (

      <View key={keyval} style={styles.note}>

        <Text style={styles.noteText}>{val.date}</Text>
        <Text style={styles.noteText}>{val.note}</Text>

        <TouchableOpacity onPress={deleteMethod} style={styles.noteDelete}>
          <Image source={require('./../../assets/icons/trash_red.png')} style={{ width:18, height: 22 }} />
        </TouchableOpacity>

      </View>
    );
};
export default Todo;

const styles = StyleSheet.create({
  note: {
    position: 'relative',
    padding: 20,
    paddingRight: 100,
    borderBottomWidth: 2,
    borderBottomColor: '#ededed',
  },
  noteText: {
    paddingLeft: 20,
    borderLeftWidth: 10,
    borderLeftColor: '#3EC6FF',
  },
  noteDelete: {
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: '#e91e63',
    padding: 10,
    top: 10,
    bottom: 10,
    right: 20
  },
});

