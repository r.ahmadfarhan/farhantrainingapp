import React from 'react';
import { View, Text, ScrollView, TextInput, TouchableOpacity, StyleSheet } from 'react-native';

import Todo from './Todo';

export default class TodoList extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      todoArray: [],
      todoText: '',
    }
  }

  render() {
    let todo = this.state.todoArray.map((val, key) => {
      return <Todo key={key} keyval={key} val = {val}
              deleteMethod={ ()=> this.deleteTodo(key) } />
    });

    return (
      <View style={styles.container}>

        <View style={styles.header}>
          <Text style={styles.headerText}>- To do List -</Text>
        </View>

        <View style={styles.form}>

          <View style={styles.input}>
            <TextInput
              style={styles.textInput}
              onChangeText={(todoText => this.setState({todoText}))}
              placeholder='Input to do here'
              placeholderTextColor='gray'
            />
          </View>
          <TouchableOpacity onPress={ this.addTodo.bind(this) } style={styles.addButton}>
            <Text style={styles.addButtonText}>+</Text>
          </TouchableOpacity>

        </View>

        <ScrollView style={styles.scrollContainer}>
          {todo}
        </ScrollView>

      </View>
    );
  }

  addTodo() {
    
    if (this.state.todoText) {

      var d = new Date();
      this.state.todoArray.push({
        'date': d.getFullYear() +
        "/" + (d.getMonth() + 1) +
        "/" + d.getDate(),
        'note': this.state.todoText
      });
      this.setState({ todoArray: this.state.todoArray})
      this.setState({ todoText: ''});

    }

  }

  deleteTodo(key) {
    this.state.todoArray.splice(key, 1);
    this.setState({ todoArray: this.state.todoArray })
  }
  
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    backgroundColor: '#3EC6FF',
    alignItems: 'center',
    justifyContent: 'center',
    borderBottomWidth: 10,
    borderBottomColor: '#ddd',
  },
  headerText: {
    color: 'black',
    fontSize: 18,
    fontWeight: 'bold',
    padding: 26,
  },
  scrollContainer: {
    flex: 1,
    marginBottom: 10,
  },
  form: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginHorizontal: 10,
    padding: 10,
  },
  input: {
    flex: 1,
  },
  textInput: {
    alignSelf: 'stretch',
    color: '#000',
    padding: 8,
    borderWidth: 1,
    marginRight: 10,
  },
  addButton: {
    backgroundColor: '#3EC6FF',
    width: 45,
    height: 45,
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 8,
  },
  addButtonText: {
    color: '#000',
    fontSize: 24,
  },
});

