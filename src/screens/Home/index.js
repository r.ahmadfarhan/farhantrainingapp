import React from 'react';
import { View, Text, Image, StatusBar, TouchableOpacity } from 'react-native';
import styles from './style';
import Icon from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { ScrollView } from 'react-native-gesture-handler';

const Home = ({navigation}) => {
    
    const onRNPress = async () => {
        try {
            navigation.navigate('Chart')
        } catch(err) {
            console.log(err)
        }
    }
    return (
        <View style={styles.container}>
            <StatusBar backgroundColor="#FFFFFF" barStyle="dark-content" />
            <ScrollView>

                <View style={styles.boxContainer}>
                    <View style={styles.boxHeader}>
                        <Text style={styles.text}>Kelas</Text>
                    </View>
                    <View style={styles.boxContent}>
                        <View style={styles.logo}>
                            <TouchableOpacity style={{alignItems: 'center'}} onPress={() => onRNPress()}>
                                <Icon name="logo-react" size={50} color='#FFFFFF'/>
                                <Text style={styles.text}>React Native</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.logo}>
                            <Icon name="logo-python" size={50} color='#FFFFFF'/>
                            <Text style={styles.text}>Data Science</Text>
                        </View>
                        <View style={styles.logo}>
                            <Icon name="logo-react" size={50} color='#FFFFFF'/>
                            <Text style={styles.text}>React JS</Text>
                        </View>
                        <View style={styles.logo}>
                            <Icon name="logo-laravel" size={50} color='#FFFFFF'/>
                            <Text style={styles.text}>Laravel</Text>
                        </View>
                    </View>
                </View>

                <View style={styles.boxContainer}>
                    <View style={styles.boxHeader}>
                        <Text style={styles.text}>Kelas</Text>
                    </View>
                    <View style={styles.boxContent}>
                        <View style={styles.logo}>
                            <Icon name="logo-wordpress" size={50} color='#FFFFFF'/>
                            <Text style={styles.text}>Wordpress</Text>
                        </View>
                        <View style={styles.logo}>
                            <Image source={require('../../assets/icons/website-design.png')} style={styles.image} />
                            <Text style={styles.text}>Design Grafis</Text>
                        </View>
                        <View style={styles.logo}>
                            <MaterialCommunityIcons name="server" size={50} color='#FFFFFF'/>
                            <Text style={styles.text}>Web Server</Text>
                        </View>
                        <View style={styles.logo}>
                            <Image source={require('../../assets/icons/ux.png')} style={styles.image} />
                            <Text style={styles.text}>UI/UX Design</Text>
                        </View>
                    </View>
                </View>

                <View style={styles.boxContainer}>
                    <View style={styles.boxHeader}>
                        <Text style={styles.text}>Summary</Text>
                    </View>

                    <View style={styles.sumHeader}>
                        <Text style={styles.text}>React Native</Text>
                    </View>
                    <View style={styles.sumTable}>
                        <View style={styles.row}>
                            <Text style={styles.text}>Today</Text>
                            <Text style={styles.text}>20 orang</Text>
                        </View>
                        <View style={styles.row}>
                            <Text style={styles.text}>Total</Text>
                            <Text style={styles.text}>100 orang</Text>
                        </View>
                    </View>

                    <View style={styles.sumHeader}>
                        <Text style={styles.text}>Data Science</Text>
                    </View>
                    <View style={styles.sumTable}>
                        <View style={styles.row}>
                            <Text style={styles.text}>Today</Text>
                            <Text style={styles.text}>30 orang</Text>
                        </View>
                        <View style={styles.row}>
                            <Text style={styles.text}>Total</Text>
                            <Text style={styles.text}>100 orang</Text>
                        </View>
                    </View>

                    <View style={styles.sumHeader}>
                        <Text style={styles.text}>React JS</Text>
                    </View>
                    <View style={styles.sumTable}>
                        <View style={styles.row}>
                            <Text style={styles.text}>Today</Text>
                            <Text style={styles.text}>66 orang</Text>
                        </View>
                        <View style={styles.row}>
                            <Text style={styles.text}>Total</Text>
                            <Text style={styles.text}>100 orang</Text>
                        </View>
                    </View>

                    <View style={styles.sumHeader}>
                        <Text style={styles.text}>Laravel</Text>
                    </View>
                    <View style={styles.sumTable}>
                        <View style={styles.row}>
                            <Text style={styles.text}>Today</Text>
                            <Text style={styles.text}>60 orang</Text>
                        </View>
                        <View style={styles.row}>
                            <Text style={styles.text}>Total</Text>
                            <Text style={styles.text}>100 orang</Text>
                        </View>
                    </View>

                    <View style={styles.sumHeader}>
                        <Text style={styles.text}>Wordpress</Text>
                    </View>
                    <View style={styles.sumTable}>
                        <View style={styles.row}>
                            <Text style={styles.text}>Today</Text>
                            <Text style={styles.text}>20 orang</Text>
                        </View>
                        <View style={styles.row}>
                            <Text style={styles.text}>Total</Text>
                            <Text style={styles.text}>100 orang</Text>
                        </View>
                    </View>

                    <View style={styles.sumHeader}>
                        <Text style={styles.text}>Design Grafis</Text>
                    </View>
                    <View style={styles.sumTable}>
                        <View style={styles.row}>
                            <Text style={styles.text}>Today</Text>
                            <Text style={styles.text}>20 orang</Text>
                        </View>
                        <View style={styles.row}>
                            <Text style={styles.text}>Total</Text>
                            <Text style={styles.text}>100 orang</Text>
                        </View>
                    </View>

                    <View style={styles.sumHeader}>
                        <Text style={styles.text}>Web Server</Text>
                    </View>
                    <View style={styles.sumTable}>
                        <View style={styles.row}>
                            <Text style={styles.text}>Today</Text>
                            <Text style={styles.text}>20 orang</Text>
                        </View>
                        <View style={styles.row}>
                            <Text style={styles.text}>Total</Text>
                            <Text style={styles.text}>100 orang</Text>
                        </View>
                    </View>

                    <View style={styles.sumHeader}>
                        <Text style={styles.text}>UI/UX Design</Text>
                    </View>
                    <View style={styles.sumTable}>
                        <View style={styles.row}>
                            <Text style={styles.text}>Today</Text>
                            <Text style={styles.text}>20 orang</Text>
                        </View>
                        <View style={styles.row}>
                            <Text style={styles.text}>Total</Text>
                            <Text style={styles.text}>100 orang</Text>
                        </View>
                    </View>

                </View>
            </ScrollView>
        </View>
    );
}
export default Home;
