import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    boxContainer: {
        marginTop: 10,
        marginHorizontal: 10,
    },
    boxHeader: {
        backgroundColor: '#088dc4',
        paddingHorizontal: 10,
        paddingVertical: 5,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10
    },
    boxContent: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 10,
        paddingVertical: 10,
        backgroundColor: '#3EC6FF',
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10
    },
    logo: {
        color: '#FFFFFF',
        alignItems: 'center'
    },
    image: {
        width: 50,
        height: 50,
    },
    text: {
        fontSize: 14,
        color: '#FFFFFF'
    },
    sumHeader: {
        backgroundColor: '#3EC6FF',
        paddingHorizontal: 10,
        paddingVertical: 5,
    },
    sumTable: {
        backgroundColor: '#088dc4',
        paddingHorizontal: 40,
        paddingVertical: 5
    },
    row: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    }
});

export default styles;
