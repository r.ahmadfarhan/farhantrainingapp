import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    imageContainer: {
        alignItems: 'center',
        marginBottom: 10
    },
    image: {
        width: 200,
        height: 200,
        marginVertical: 20
    },
    content: {
        marginHorizontal: 20
    },
    formContainer: {
        marginVertical: 10
    },
    btnContainer: {
        padding: 5
    },
    btnFingerprint: {
        marginTop: 10,
        padding: 5
    },
    regContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginHorizontal: 20,
        marginTop: 30
    },
})

export default styles;