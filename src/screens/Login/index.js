import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    Image,
    Button,
    TextInput,
    StatusBar,
    TouchableOpacity
} from 'react-native';
import styles from './style';
import Axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage'
import api from '../../api'
import { ScrollView } from 'react-native-gesture-handler';
import auth from '@react-native-firebase/auth';
import { GoogleSignin, statusCodes, GoogleSigninButton } from '@react-native-community/google-signin';
import TouchID from 'react-native-touch-id';

const config = {
    title: 'Authentication.Required',
    imageColor: '#191970',
    imageErrorColor: 'red',
    sensorDescription: 'Touch Sensor',
    sensorErrorDescription: 'Failed',
    cancelText: 'Cancel'
}

function Login({ navigation }) {

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const saveToken = async (token) => {
        try {
            await AsyncStorage.setItem("token", token)
        } catch (err) {
            console.log(err)
        }
    }

    useEffect(() => {
        configureGoogleSignIn()
    }, [])

    const configureGoogleSignIn = () => {
        GoogleSignin.configure({
            offlineAccess: false,
            webClientId: '1070759017540-tlnvjuo7dt06p49r1l7ackr4ursfqt12.apps.googleusercontent.com'
        })
    }

    const resetNavigation = () => {
        navigation.reset({
            index: 0, 
            routes: [{ name: 'Dashboard' }]
        })


    }

    const signInWithGoogle = async () => {
        try {
            const { idToken } = await GoogleSignin.signIn()
            console.log("signInWithGoogle -> idToken", idToken)

            const credential = auth.GoogleAuthProvider.credential(idToken)

            auth().signInWithCredential(credential)

            navigation.navigate('Dashboard')

        } catch (error) {
            console.log("signInWithGoogle -> error", error)

        }
    }

    const onLoginPress = () => {
        return auth().signInWithEmailAndPassword(email, password)
        .then((res) => {
            // navigation.navigate('Dashboard')
            resetNavigation()
            saveToken('1')
        })
        .catch((err) => {
            console.log("onLoginPress -> error", err)
        })
    }

    // const onLoginPress = () => {
    //     let data = {
    //         email: email,
    //         password: password
    //     }
    //     Axios.post(`${api}/login`, data, {
    //         timeout: 20000
    //     })
    //     .then((res) => {
    //         console.log("login -> res", res)
    //         saveToken(res.data.token)
    //         navigation.navigate('Profile')
    //     })
    //     .catch((err) => {
    //         console.log("Login -> err", err)
    //     })
    // }

    const signInWithFingerprint = () => {
        TouchID.authenticate('', config)
        .then(success => {
            navigation.navigate('Profile')
            // alert('Authentication Success')
        })
        .catch(error => {
            alert('Authentication Failed')
            console.log("Login -> err", error)
        })
    }

    return (
        <View style={styles.container}>
            <StatusBar backgroundColor="#ffffff" barStyle="dark-content" />
            <ScrollView>
                <View style={styles.imageContainer}>
                    <Image source={require('../../assets/images/logo.png')} style={styles.image} />
                </View>
                <View style={styles.content}>
                    <View style={styles.formContainer}>
                        <Text>Username</Text>
                        <TextInput
                            value={email}
                            underlineColorAndroid= '#c6c6c6'
                            placeholder='Username or Email'
                            onChangeText={(email)=>setEmail(email)}
                        />
                        <Text>Password</Text>
                        <TextInput
                            secureTextEntry
                            value={password}
                            underlineColorAndroid= '#c6c6c6'
                            placeholder='Password'
                            onChangeText={(password)=>setPassword(password)}
                        />
                    </View>
                    <View style={styles.btnContainer}>
                        <View>
                            <Button 
                                color='#3ec6ff'
                                title='LOGIN'
                                onPress={() => onLoginPress()}
                            />
                        </View>
                    </View>
                    <View style={{marginTop: 10, alignItems: 'center'}}>
                        <Text>OR</Text>
                    </View>
                    <View style={{marginTop: 10}}>
                        <GoogleSigninButton
                            onPress={() => signInWithGoogle()}
                            style={{ width: '100%', height: 40}}
                            size={GoogleSigninButton.Size.Wide}
                            color={GoogleSigninButton.Color.Dark}
                        />
                    </View>
                    <View style={styles.btnFingerprint}>
                        <Button 
                            color='#191970'
                            title='Sign in with Fingerprint'
                            onPress={() => signInWithFingerprint()}
                        />
                    </View>
                </View>
                <View style={styles.regContainer}>
                    <Text>Belum mempunyai akun ? </Text>
                    <TouchableOpacity onPress={() => navigation.navigate('Register')}>
                        <Text>Buat Akun</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        </View>
    )
}

export default Login;