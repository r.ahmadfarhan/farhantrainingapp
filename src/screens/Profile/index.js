import React, { useEffect, useState } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  StatusBar,
  TouchableOpacity
} from 'react-native';
import Axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import api from '../../api';
import { ScrollView } from 'react-native-gesture-handler';
import { GoogleSignin } from '@react-native-community/google-signin';

const Profile = ({navigation}) => {

    const [userInfo, setUserInfo] = useState(null)

    // useEffect(() => {
    //    async function getToken() {
    //        try {
    //            const token = await AsyncStorage.getItem("token")
    //         //    return getVenue(token)
    //         //    console.log("getToken -> token", token)
    //        } catch(err) {
    //            console.log(err)
    //        }
    //    }
    //    getToken()
    //    getCurrentUser()
    // }, [userInfo])

    const getVenue = (token) => {
        Axios.get(`${api}/venues`, {
            timeout: 20000,
            headers: {
                'Authorization:' : 'Bearer' + token
            }
        })
        .then((res) => {
            console.log("Profile -> res", res)
        })
        .catch((err) => {
            console.log("Profile -> err", err)
        })
    }

    const getCurrentUser = async () => {
        const userInfo = await GoogleSignin.signInSilently()
        setUserInfo(userInfo)
    }

    const onLogoutPress = async () => {
        try {
            await GoogleSignin.revokeAccess()
            await GoogleSignin.signOut()
            await AsyncStorage.removeItem("token")
            navigation.navigate('Login')
        } catch(err) {
            console.log(err)
        }
    }

    return (
        <View style={styles.container}>
            <StatusBar backgroundColor="#3EC6FF" barStyle="dark-content" />
            <ScrollView>
            <View style={styles.background}>
                <View style={styles.photo}>
                    {/* <Image source={{uri:userInfo && userInfo.user && userInfo.user.photo}} style={{ width:100, height: 100, borderRadius: 50 }} /> */}
                    <Image source={require('./../../assets/images/pp.jpg')} style={{ width:100, height: 100, borderRadius: 50 }} />
                </View>
                <View style={styles.name}> 
                    {/* <Text style={styles.nameText}>{userInfo && userInfo.user && userInfo.user.name}</Text>   */}
                    <Text style={styles.nameText}>R Ahmad Farhan</Text>  
                </View>
            </View>
            <View style={styles.box}>
                <View style={styles.row}>
                    <Text style={styles.text}>Tanggal lahir</Text>
                    <Text style={styles.text}>27 Jan 1990</Text>
                </View>
                <View style={styles.row}>
                    <Text style={styles.text}>Jenis Kelamin</Text>
                    <Text style={styles.text}>Laki-laki</Text>
                </View>
                <View style={styles.row}>
                    <Text style={styles.text}>Hobi</Text>
                    <Text style={styles.text}>Ngoding</Text>
                </View>
                <View style={styles.row}>
                    <Text style={styles.text}>No. Telp</Text>
                    <Text style={styles.text}>085759889974</Text>
                </View>
                <View style={styles.row}>
                    <Text style={styles.text}>Email</Text>
                    {/* <Text style={styles.text}>{userInfo && userInfo.user && userInfo.user.email}</Text> */}
                    <Text style={styles.text}>r.ahmadfarhan@gmail.com</Text>
                </View>
                <View style={styles.btnContainer}>
                    <TouchableOpacity style={styles.logout} onPress={() => onLogoutPress()}>
                        <Text style={{color: '#ffffff'}}>LOGOUT</Text>
                    </TouchableOpacity>
                </View>
            </View>
            </ScrollView>
        </View>
    );
}
export default Profile;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  background: {
    backgroundColor: '#3EC6FF',
    height: 250,
  },
  photo: {
    alignItems: 'center',
    marginTop: 50,
  },
  name: {
    alignItems: 'center',
  },
  nameText: {
    fontSize: 18,
    lineHeight: 21,
    fontWeight: 'bold',
    marginVertical: 10,
    color: '#FFFFFF'
  },
  box: {
    backgroundColor: '#FFFFFF',
    borderRadius: 10,
    marginTop: -40,
    marginHorizontal: 20,
    paddingVertical: 13,
    paddingHorizontal: 10,
    shadowColor: "#000",
    shadowOffset: {width: 1, height: 3,},
    shadowOpacity: 0.3,
    shadowRadius: 6,
    elevation: 10,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: 20,
    marginVertical: 10
  },
  text: {
    fontSize: 14,
    color: '#000000'
  },
  btnContainer: {
    margin: 15,
    backgroundColor: '#3EC6FF',
    alignItems: 'center'
  },
  logout: {
    fontSize: 14,
    marginVertical: 10
  }
});