import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  StatusBar
} from 'react-native';

const Profile = () => {
    return (
        <View style={styles.container}>
            <StatusBar barStyle="light-content" />
            <View style={styles.background}>
                <View style={styles.photo}>
                    <Image source={require('./../../assets/images/pp.jpg')} style={{ width:100, height: 100 }} />
                </View>
                <View style={styles.name}> 
                    <Text style={styles.nameText}>R Ahmad Farhan</Text>  
                </View>
                <View style={styles.box}>
                    <View style={styles.row}>
                        <Text style={styles.text}>Tanggal lahir</Text>
                        <Text style={styles.text}>27 Jan 1990</Text>
                    </View>
                    <View style={styles.row}>
                        <Text style={styles.text}>Jenis Kelamin</Text>
                        <Text style={styles.text}>Laki-laki</Text>
                    </View>
                    <View style={styles.row}>
                        <Text style={styles.text}>Hobi</Text>
                        <Text style={styles.text}>Ngoding</Text>
                    </View>
                    <View style={styles.row}>
                        <Text style={styles.text}>No. Telp</Text>
                        <Text style={styles.text}>085759889974</Text>
                    </View>
                    <View style={styles.row}>
                        <Text style={styles.text}>Email</Text>
                        <Text style={styles.text}>r.ahmadfarhan@gmail.com</Text>
                    </View>
                </View>
            </View>
        </View>
    );
}
export default Profile;

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  background: {
    backgroundColor: '#3EC6FF',
    height: 270,
    alignItems: 'center',
  },
  photo: {
    alignItems: 'center',
    marginTop: 70,
    borderRadius: 100,
  },
  name: {
    alignItems: 'center',
  },
  nameText: {
    fontSize: 18,
    lineHeight: 21,
    fontWeight: 'bold',
    marginVertical: 10,
    color: '#FFFFFF'
  },
  box: {
    backgroundColor: '#FFFFFF',
    borderRadius: 10,
    width: 365,
    marginTop: 15,
    paddingVertical: 13,
    paddingHorizontal: 10,
    shadowColor: "#000",
    shadowOffset: {width: 1, height: 3,},
    shadowOpacity: 0.3,
    shadowRadius: 6,
    elevation: 10,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: 20,
    marginVertical: 10
  },
  text: {
    fontSize: 14,
    color: '#000000'
  },
});