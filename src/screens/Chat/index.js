import React, { useEffect, useState } from 'react';
import { View, Text, processColor, StatusBar } from 'react-native';
import auth from '@react-native-firebase/auth';
import database from '@react-native-firebase/database';
import { GiftedChat } from 'react-native-gifted-chat'

const Chat = ({ route, navigation }) => {

    const [messages, setMessages] = useState([])
    const [user, setUser] = useState({})

    useEffect(() => {
        const user = auth().currentUser;
        // console.log("Chat -> user", user);
        setUser(user)
        getData()
        return () => {
            const db = database().ref('messages')
            if(db) {
                db.off()
            }
        }
    }, [])

    const getData = () => {
        database().ref('messages').limitToLast(20).on('child_added', snapshot => {
            // console.log("getData -> snapshot", snapshot)
            const value = snapshot.val()
            // console.log("getData -> value", value)
            setMessages(previousMessages => GiftedChat.append(previousMessages, value))

        })
    }

    const onSend = (( messages = [] ) => {
        // console.log("onSend -> messages", messages)
        for (let i = 0; i < messages.length; i++) {
            database().ref('messages').push({
                _id: messages[i]._id,
                createdAt: database.ServerValue.TIMESTAMP,
                text: messages[i].text,
                user: messages[i].user
            })
            
        }
    })

    return (
        <GiftedChat 
            messages={messages}
            onSend={messages => onSend(messages)}
            user={{
                _id: user.uid,
                name: user.email,
                avatar: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRPEqNZVkXW08m9IA-26NOo4HC8A5Pya1fhvQ&usqp=CAU',
            }}
        />
    )
}

export default Chat;
