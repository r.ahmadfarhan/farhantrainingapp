/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
// import Intro from './src/screens/Tugas1/Intro';
// import Profile from './src/screens/Tugas2/Profile';
// import HooksTodoList from './src/screens/Tugas3/TodoList';
// import ContextTodoList from './src/screens/Tugas4/index';
// import Onboarding from './src/navigation/navigation';
// import JWT from './src/navigation/Tugas7';
// import AuthGoogle from './src/App';
// import Home from './src/navigation/Tugas11';
// import Maps from './src/navigation/Tugas12';
// import Chart from './src/navigation/Tugas13';
// import Chat from './src/navigation/Tugas14';
import ComplexNavigation from './src/navigation/Tugas15';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => ComplexNavigation);
